package nl.knmi.c4i.data.esgf;

import nl.knmi.c4i.data.esgf.config.*;
import nl.knmi.c4i.data.esgf.exceptions.EndpointNotValidException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.net.MalformedURLException;
import java.util.LinkedList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EsgfConfigTests {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void whenFirstValidEndpointCalled_shouldReturnFirstValidEndpoint(){
        // Arrange
        IEndpointConnector endpointConnector = mock(IEndpointConnector.class);
        IEndpointValidator endpointValidator200 = new EndpointValidator(endpointConnector);

       final String urlReturns404 = "https://notworking.com/"; // 404
       final String expected = "https://working.com/"; // 200

        Answer<Integer> answer = invocationOnMock -> {
            String url = (String) invocationOnMock.getArguments()[0];
            switch (url) {
                case expected:
                    return 200;
                case urlReturns404:
                    return 404;
            }
            return 500;
        };

        try {
            when(endpointConnector.getEndpointStatusCode(anyString())).thenAnswer(answer);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        LinkedList<String> list = new LinkedList<>();
        list.add(urlReturns404);
        list.add(expected);

        // Act
        String actual;
        try {
            actual = endpointValidator200.getFirstValidEndpoint(list);

            // Assert
            assertThat(actual).isEqualTo(expected);
        } catch (EndpointNotValidException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void whenEndpointNotValid_shouldThrowEndpointNotValidException() throws EndpointNotValidException {
        exceptionRule.expect(EndpointNotValidException.class);

        // Arrange
        IEndpointConnector endpointConnector = mock(IEndpointConnector.class);
        IEndpointValidator endpointValidator = new EndpointValidator(endpointConnector);

       final String urlNotValid = "https://notavalidurl.com/";

        try {
            when(endpointConnector.getEndpointStatusCode(anyString())).thenReturn(404);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        LinkedList<String> list = new LinkedList<>();
        list.add(urlNotValid);

        // Act
        String actual = endpointValidator.getFirstValidEndpoint(list);

        // Assert done by exceptionRule
    }

    @Test
    public void whenGivenMalformedURL_shouldThrowMalformedURLException() throws MalformedURLException {
        exceptionRule.expect(MalformedURLException.class);

        // Arrange
        IEndpointConnector endpointConnector = new EndpointConnector();
        final String urlNotValid = "malformed url";

        // Act
        endpointConnector.getEndpointStatusCode(urlNotValid);

        // Assert done by exceptionRule
    }
}