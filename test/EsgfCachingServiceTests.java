import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.dto.FacetPropertyDTO;
import nl.knmi.c4i.data.esgf.request.SearchQuery;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collection;

@RunWith(SpringRunner.class)
public class EsgfCachingServiceTests {
    // For if you are to lazy to create it yourself
    private String queryBuilder(String fp, String wl, String bl, boolean s, int l, int o) {
        // Convert the boolean to a string
        String ss;
        if(s) ss = "true";
        else ss = "false";

        // Return the string
        return "fp=" + fp + "&wl=" + wl + "&bl=" + bl + "&s=" + ss + "&l=" + l + "&o=" + o;
    }

    @Test
    public void whenQueryIsCreatedWithDefaultProperties_thenIdShouldBeDefault() {
        // Create the default query. Taken from SearchQuery.java line 27
        SearchQuery queryDefault = new SearchQuery(new ArrayList<>(), 25, 0, new ArrayList<>(), new ArrayList<>(), true);

        String expected = queryBuilder("", "", "", true, 25, 0);
        String actual = queryDefault.getId();

        Assert.assertEquals("Check if query id is same as expected", expected, actual);
    }

    @Test
    public void whenQueryIsCreatedWithProperties_thenIdShouldMatch() {
        // Creating the names
        String propertyName = "testProperty";
        String facetName = "testFacet";

        // Creating the facet lists for the black and whitelist
        FacetDTO whiteFacet = new FacetDTO(facetName + "white");
        FacetDTO blackFacet = new FacetDTO(facetName + "black");
        Collection<FacetDTO> whiteFacets = new ArrayList<>();
        Collection<FacetDTO> blackFacets = new ArrayList<>();
        whiteFacets.add(whiteFacet);
        blackFacets.add(blackFacet);

        // Creating the custom facetProperties
        FacetPropertyDTO propertyDTO = new FacetPropertyDTO(propertyName, facetName);
        Collection<FacetPropertyDTO> facetProperties = new ArrayList<>() ;
        facetProperties.add(propertyDTO);

        // Creating a query with custom properties
        SearchQuery query = new SearchQuery(facetProperties, 50, 5, whiteFacets, blackFacets, false);
        String fp = facetName + "[" + propertyName + "]";
        String expected = queryBuilder(fp, facetName + "white", facetName + "black", false, 50, 5);
        String actual = query.getId();

        Assert.assertEquals("Check if with custom facetProperties, query id is same as expected", expected, actual);
    }

    @Test
    public void whenQueryIsCreatedWithDifferentFacetProperties_thenFacetPropertiesShouldBeSortedAlphabetically() {
        // Creating the different names
        String propertyName1 = "testPropertyC";
        String propertyName2 = "testPropertyA";
        String propertyName3 = "testPropertyB";
        String facetName = "testFacet";

        // Creating the different facetProperties with each having a different name
        FacetPropertyDTO propertyDTO1 = new FacetPropertyDTO(propertyName1, facetName);
        FacetPropertyDTO propertyDTO2 = new FacetPropertyDTO(propertyName2, facetName);
        FacetPropertyDTO propertyDTO3 = new FacetPropertyDTO(propertyName3, facetName);
        Collection<FacetPropertyDTO> facetProperties = new ArrayList<>() ;
        facetProperties.add(propertyDTO1);
        facetProperties.add(propertyDTO2);
        facetProperties.add(propertyDTO3);

        // Creating a query with custom facetProperties
        SearchQuery query = new SearchQuery(facetProperties, 50, 5, new ArrayList<>(), new ArrayList<>(), false);
        String fp = facetName + "[" + propertyName2 + "," + propertyName3 + "," + propertyName1 + "]";
        String expected = queryBuilder(fp, "", "", false, 50, 5);
        String actual = query.getId();

        Assert.assertEquals("Check if with custom facetProperties, query id is sorted alphabetically", expected, actual);
    }

    @Test
    public void whenQueryIsCreatedWithDifferentFacets_thenFacetsShouldBeSortedAlphabetically() {
        // Creating the different names
        String propertyName = "testProperty";
        String facetName1 = "testFacetC";
        String facetName2 = "testFacetA";
        String facetName3 = "testFacetB";

        // Creating the different facetProperties with each having a facet which each have a different name
        FacetPropertyDTO propertyDTO1 = new FacetPropertyDTO(propertyName, facetName1);
        FacetPropertyDTO propertyDTO2 = new FacetPropertyDTO(propertyName, facetName2);
        FacetPropertyDTO propertyDTO3 = new FacetPropertyDTO(propertyName, facetName3);
        Collection<FacetPropertyDTO> facetProperties = new ArrayList<>() ;
        facetProperties.add(propertyDTO1);
        facetProperties.add(propertyDTO2);
        facetProperties.add(propertyDTO3);

        // Creating a query with custom facets
        SearchQuery query = new SearchQuery(facetProperties, 50, 5, new ArrayList<>(), new ArrayList<>(), false);
        String fp = facetName2 + "[" + propertyName + "]," +
                    facetName3 + "[" + propertyName + "]," +
                    facetName1 + "[" + propertyName + "]";
        String expected = queryBuilder(fp, "", "", false, 50, 5);
        String actual = query.getId();

        Assert.assertEquals("Check if with custom facets, query id is sorted alphabetically", expected, actual);
    }
}
