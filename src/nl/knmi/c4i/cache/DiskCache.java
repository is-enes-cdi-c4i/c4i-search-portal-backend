package nl.knmi.c4i.cache;

import nl.knmi.c4i.util.Debug;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Calendar;
import java.util.stream.Collectors;

public class DiskCache implements ICache<String> {

    private final String cacheLocation;
    private final int maxAgeInSeconds;

    /**
     * @param diskCacheLocation Location of cache on the disk
     * @param maxAgeInSeconds   Maximum allowed age for cached items
     */
    public DiskCache(String diskCacheLocation, int maxAgeInSeconds) {
        this.cacheLocation = diskCacheLocation;
        this.maxAgeInSeconds = maxAgeInSeconds;
    }

    @Override
    public boolean isCacheValid(String identifier) {
        Path fileCacheId = getCacheFile(identifier).toPath();

        BasicFileAttributes attributes;
        try {
            attributes = Files.readAttributes(fileCacheId, BasicFileAttributes.class);
        } catch (IOException e) {
            return false;
        }
        FileTime creationTime = attributes.lastModifiedTime();
        long ageInSeconds = (Calendar.getInstance()
                                     .getTimeInMillis() - creationTime.toMillis()) / 1000;

        return ageInSeconds <= maxAgeInSeconds;
    }

    /**
     * @param identifier Unique id for the cached message
     *
     * @return the stored message, null if not available or too old
     */
    @Override
    public String get(String identifier) throws IOException {
        if (!isCacheValid(identifier)) return null;

        File file = getCacheFile(identifier);

        if (!file.exists()) throw new FileNotFoundException();

        BufferedReader reader = new BufferedReader(new FileReader(file));

        String data = reader.lines()
                            .collect(Collectors.joining());
        reader.close();
        return data;
    }

    /**
     * Store a string in the diskcache system identified with an id
     *
     * @param data       The data to store
     * @param identifier The identifier of this string
     */
    @Override
    public void set(String identifier, String data) throws IOException {
        Path cacheDirectory = Paths.get(cacheLocation);
        if (!Files.exists(cacheDirectory)) {
            Files.createDirectories(cacheDirectory);
        }

        try {
            PrintWriter fileWriter = new PrintWriter(getCacheFile(identifier));
            fileWriter.print(data);
            fileWriter.close();
        } catch (IOException e) {
            Debug.errprintln("Unable to write to cachelocation " + cacheLocation + " with identifier " + identifier);
            throw e;
        }
    }

    private File getCacheFile(String identifier) {
        identifier = sanitizeId(identifier);
        Path cacheDirectory = Paths.get(cacheLocation);

        return new File(cacheDirectory + "/" + identifier);
    }

    @Override
    public void invalidateCache(String identifier) {
        identifier = sanitizeId(identifier);
        Path cacheDirectory = Paths.get(cacheLocation);
        if (!Files.exists(cacheDirectory)) return;

        try {
            Files.delete(Paths.get(cacheDirectory + "/" + identifier));
        } catch (IOException ignored) {}
    }


    /**
     * @param id Id to sanitize
     *
     * @return Sanitized id
     */
    private String sanitizeId(String id) {
        final String[] illegalChars = {"\\?", "&", ":", "/", "="};
        final String replacementChar = "_";

        for (String illegalChar : illegalChars) {
            id = id.replaceAll(illegalChar, replacementChar);
        }
        return id;
    }

}
