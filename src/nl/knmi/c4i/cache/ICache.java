package nl.knmi.c4i.cache;

import java.io.IOException;

/**
 * @param <TItem> Type of the cached items
 */
public interface ICache<TItem> {
    /**
     * @param key Identifier for the item
     *
     * @return whether cache is present and valid
     */
    boolean isCacheValid(String key);

    /**
     * @param key Identifier for the item
     *
     * @return item if present, null if not
     */
    TItem get(String key) throws IOException;

    /**
     * @param key  Identifier for the item
     * @param item Item to cache
     */
    void set(String key, TItem item) throws IOException;

    /**
     * Invalidates cache-item
     *
     * @param key Identifier for the item to be invalidated
     */
    void invalidateCache(String key);

}
