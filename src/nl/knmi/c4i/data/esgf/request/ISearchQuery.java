package nl.knmi.c4i.data.esgf.request;

import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.dto.FacetPropertyDTO;

import java.util.Collection;

public interface ISearchQuery {

    /** @return facet-properties that filter the results */
    Collection<FacetPropertyDTO> getFacetProperties();

    /** facet-properties that filter the results */
    void setFacetProperties(Collection<FacetPropertyDTO> facetProperties);

    /** @return facets that need to be returned */
    Collection<FacetDTO> getFacetWhitelist();

    /** @return facets that need to be returned */
    Collection<FacetDTO> getFacetBlacklist();

    /**
     * @return how many files results should at most be returned
     *
     * @see <a href='https://github.com/ESGF/esgf.github.io/wiki/ESGF_Search_REST_API#results-pagination'>ESGF docs</a>
     */
    int getLimit();

    /**
     * @return the offset that determines the 'page' for the request.
     *         For example: an offset of 25 with a limit of 5 would return files 25-30.
     *
     * @see <a href='https://github.com/ESGF/esgf.github.io/wiki/ESGF_Search_REST_API#results-pagination'>ESGF docs</a>
     */
    int getOffset();

    /**
     * @return whether or not the results returned should be sorted
     *
     * @see <a href='https://github.com/ESGF/esgf.github.io/wiki/ESGF_Search_REST_API#sorting'>ESGF docs</a>
     */
    boolean getSort();

    String getId();
}
