package nl.knmi.c4i.data.esgf.request;

import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.dto.FacetPropertyDTO;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UrlFactory implements IUrlFactory {
    /**
     * @param endpoint which search-endpoint to suffix the url behind
     * @param query    the query-model to change to
     *
     * @return valid esgf-search-url
     */
    @Override
    public String createSearchUrl(String endpoint, ISearchQuery query) {
        return endpoint + this.createSearchUrlQuery(query);
    }

    @Override
    public String createSearchUrlQuery(ISearchQuery query) {

        Collection<String> facetWhitelistNames = query.getFacetWhitelist()
                                                      .stream()
                                                      .map(FacetDTO::getShortName)
                                                      .collect(Collectors.toList());

        Function<String, String> encode = (String toEncode) -> {
            try {
                return URLEncoder.encode(toEncode, StandardCharsets.UTF_8.name());
            } catch (UnsupportedEncodingException e) {
                return toEncode;
            }
        };

        Map<String, String> queryMap = new HashMap<>();
        queryMap.put("format", "application/solr+json");
        queryMap.put("facets", !facetWhitelistNames.isEmpty() ? String.join(",", facetWhitelistNames) : "*");
        queryMap.put("limit", String.valueOf(query.getLimit()));
        queryMap.put("offset", String.valueOf(query.getOffset()));
        query.getFacetProperties()
             .stream()
             .sorted(Comparator.comparing(FacetPropertyDTO::getName))
             .sorted(Comparator.comparing(FacetPropertyDTO::getFacetName))
             .forEach(property -> queryMap.put(property.getFacetName(), property.getName()));

        return "?" + queryMap.entrySet()
                             .stream()
                             .map((Map.Entry<String, String> entry) -> entry.getKey() + "=" + encode.apply(entry.getValue()))
                             .collect(Collectors.joining("&"));
    }

}
