package nl.knmi.c4i.data.esgf.request;

import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.dto.FacetPropertyDTO;

import java.util.*;
import java.util.stream.Collectors;

public class SearchQuery implements ISearchQuery {

    private Collection<FacetDTO> facetWhitelist;
    private Collection<FacetDTO> facetBlacklist;
    private Collection<FacetPropertyDTO> facetProperties;
    private int limit, offset;
    private boolean doSort;

    public SearchQuery(Collection<FacetPropertyDTO> facetProperties, int limit, int offset, Collection<FacetDTO> facetWhitelist, Collection<FacetDTO> facetBlacklist, boolean doSort) {
        this.facetProperties = facetProperties;
        this.limit = limit;
        this.offset = offset;
        this.facetWhitelist = facetWhitelist;
        this.facetBlacklist = facetBlacklist;
        this.doSort = doSort;
    }

    public static SearchQuery emptyQuery() {
        return new SearchQuery(new ArrayList<>(), 25, 0, new ArrayList<>(), new ArrayList<>(), true);
    }

    @Override
    public Collection<FacetPropertyDTO> getFacetProperties() {
        return this.facetProperties;
    }

    @Override
    public void setFacetProperties(Collection<FacetPropertyDTO> facetProperties) {
        this.facetProperties = facetProperties;
    }

    @Override
    public int getLimit() {
        return limit;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public boolean getSort() {
        return doSort;
    }

    @Override
    public String getId() {
        String facetPropertyKey, whitelistKey, blacklistKey, sortKey, limitKey, offsetKey;

        Map<String, List<String>> groupedFacets = new HashMap<>();

        getFacetProperties().forEach(facetPropertyDTO -> {
            if (!groupedFacets.containsKey(facetPropertyDTO.getFacetName()))
                groupedFacets.put(facetPropertyDTO.getFacetName(), new ArrayList<>());

            groupedFacets.get(facetPropertyDTO.getFacetName())
                         .add(facetPropertyDTO.getName());
        });

        facetPropertyKey = "fp=" + groupedFacets.entrySet()
                                                .stream()
                                                .map(entry -> entry.getKey() + "[" + entry.getValue()
                                                                                          .stream()
                                                                                          .sorted()
                                                                                          .collect(Collectors.joining(",")) + "]")
                                                .sorted()
                                                .collect(Collectors.joining(","));

        whitelistKey = "wl=" + this.getFacetWhitelist()
                                   .stream()
                                   .map(FacetDTO::getShortName)
                                   .sorted()
                                   .collect(Collectors.joining(","));

        blacklistKey = "bl=" + this.getFacetBlacklist()
                                   .stream()
                                   .map(FacetDTO::getShortName)
                                   .sorted()
                                   .collect(Collectors.joining(","));
        sortKey = "s=" + getSort();
        limitKey = "l=" + getLimit();
        offsetKey = "o=" + getOffset();

        return String.join("&", facetPropertyKey, whitelistKey, blacklistKey, sortKey, limitKey, offsetKey);
    }

    public Collection<FacetDTO> getFacetWhitelist() {
        return facetWhitelist;
    }

    @Override
    public Collection<FacetDTO> getFacetBlacklist() {
        return facetBlacklist;
    }
}
