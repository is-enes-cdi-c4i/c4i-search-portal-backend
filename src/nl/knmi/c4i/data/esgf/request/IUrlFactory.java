package nl.knmi.c4i.data.esgf.request;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

public interface IUrlFactory {
    String createSearchUrl(String endpoint, ISearchQuery query) throws MalformedURLException, UnsupportedEncodingException;

    String createSearchUrlQuery(ISearchQuery query) throws UnsupportedEncodingException;
}
