package nl.knmi.c4i.data.esgf;

import nl.knmi.c4i.data.esgf.request.ISearchQuery;
import org.json.JSONObject;

import java.io.IOException;


public interface IEsgfService {
    JSONObject fetch(ISearchQuery query) throws IOException;
}
