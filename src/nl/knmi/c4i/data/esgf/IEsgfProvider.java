package nl.knmi.c4i.data.esgf;

import nl.knmi.c4i.data.esgf.exceptions.InvalidCacheException;
import nl.knmi.c4i.data.esgf.request.ISearchQuery;
import nl.knmi.c4i.data.esgf.response.SearchResponse;

import java.io.IOException;

public interface IEsgfProvider {
    SearchResponse provide(ISearchQuery query) throws InvalidCacheException, IOException;
}
