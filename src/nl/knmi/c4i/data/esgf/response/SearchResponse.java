package nl.knmi.c4i.data.esgf.response;

import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.dto.SearchResultDTO;

import java.util.Collection;

public class SearchResponse implements ISearchResponse {
    private Collection<FacetDTO> facets;
    private SearchResultDTO searchResult;

    public SearchResponse(Collection<FacetDTO> facets, SearchResultDTO searchResult) {
        this.facets = facets;
        this.searchResult = searchResult;
    }

    public Collection<FacetDTO> getFacets() {
        return facets;
    }

    @Override
    public SearchResultDTO getSearchResult() {
        return searchResult;
    }
}
