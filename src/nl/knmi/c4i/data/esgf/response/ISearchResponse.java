package nl.knmi.c4i.data.esgf.response;

import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.dto.SearchResultDTO;

import java.util.Collection;

public interface ISearchResponse {
    Collection<FacetDTO> getFacets();

    SearchResultDTO getSearchResult();
}
