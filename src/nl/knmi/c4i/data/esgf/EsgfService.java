package nl.knmi.c4i.data.esgf;

import nl.knmi.c4i.data.esgf.config.IEsgfConfig;
import nl.knmi.c4i.data.esgf.dto.FacetPropertyDTO;
import nl.knmi.c4i.data.esgf.request.ISearchQuery;
import nl.knmi.c4i.data.esgf.request.IUrlFactory;
import nl.knmi.c4i.util.Debug;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class EsgfService implements IEsgfService {

    private final IUrlFactory urlFactory;
    private final IEsgfConfig config;
    private String searchEndPoint = "https://esgf-data.dkrz.de/esg-search/search";
//    private String searchEndPoint = "https://index.mips.copernicus-climate.eu/esg-search/search";

    public EsgfService(IUrlFactory urlFactory, IEsgfConfig config) {
        this.urlFactory = urlFactory;
        this.config = config;
    }

    /**
     * @param query         Contains the data that is used to construct the query that is sent to ESGF
     *
     * @return              JSON object
     *
     * @throws IOException  When something goes wrong while creating SearchUrl
     */
    @Override
    public JSONObject fetch(ISearchQuery query) throws IOException {
        String queryUrl = null;
        query.setFacetProperties(this.ensureDefaults(query.getFacetProperties()));
        try {
            queryUrl = this.urlFactory.createSearchUrl(this.searchEndPoint, query);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw e;
        }

        StopWatch stopwatch = new StopWatch();
        stopwatch.start();
        Debug.println("Fetching from " + URLDecoder.decode(queryUrl, StandardCharsets.UTF_8.name()));
        String json = fetchJson(queryUrl);
        stopwatch.stop();
        Debug.println("Done fetching, request took " + stopwatch.getLastTaskTimeMillis() + " milliseconds");

        return new JSONObject(json);
    }


    private Collection<FacetPropertyDTO> ensureDefaults(Collection<FacetPropertyDTO> incomingFacets) {
        List<FacetPropertyDTO> currentFacets = new ArrayList<>(incomingFacets); //Copy the array because immutability

        Predicate<String> facetExists = facet -> currentFacets.stream()
                                                              .map(FacetPropertyDTO::getFacetName)
                                                              .collect(Collectors.toList())
                                                              .contains(facet);
        Predicate<String> facetIsEmpty = facet -> currentFacets.stream()
                                                               .filter(currentFacet -> currentFacet.getFacetName()
                                                                                                   .equalsIgnoreCase(facet))
                                                               .map(FacetPropertyDTO::getName)
                                                               .allMatch(String::isEmpty);

        this.config.getFacetDefaults()
                   .entrySet()
                   .stream()
                   .filter(facet -> facetExists.test(facet.getKey()) && !facetIsEmpty.test(facet.getKey()))
                   .forEach(facet -> currentFacets.add(new FacetPropertyDTO(facet.getValue(), facet.getKey())));

        return currentFacets;
    }

    //    private String fetchXml() {
//        String cachedXML = diskCache.get(identifier + ".xml");
//        String XML = null;
//
//        if (cachedXML == null) {
//            String url = searchEndPoint + esgfQuery;
//            try {
//                XML = HttpTools.makeHTTPGetRequestWithTimeOut(url, searchGetTimeOutMS);
//                diskCache.set(identifier + ".xml", XML);
//            } catch (MalformedURLException e2) {
//                r.setException("MalformedURLException", e2, url);
//                return r;
//            } catch (IOException e2) {
//                r.setException("IOException", e2, url);
//                return r;
//            }
//        }
//    }

    /**
     * @param esgfQueryUrl the url from which to fetch json
     *
     * @return json in string-form
     *
     * @throws IOException when something goes wrong with the connection
     */
    private String fetchJson(String esgfQueryUrl) throws IOException {
        URL url = new URL(esgfQueryUrl);

        HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
        httpConnection.setRequestMethod("GET");
        httpConnection.setRequestProperty("Accept", "application/json");

        httpConnection.connect();

        int status = httpConnection.getResponseCode();

        if (status > 299) {
            throw new RuntimeException(HttpStatus.valueOf(status)
                                                 .toString());//TODO HttpStatusCodeException(int code)
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(httpConnection.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        httpConnection.disconnect();

        return content.toString();
    }
}
