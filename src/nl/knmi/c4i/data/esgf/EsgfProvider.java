package nl.knmi.c4i.data.esgf;

import nl.knmi.c4i.cache.DiskCache;
import nl.knmi.c4i.cache.ICache;
import nl.knmi.c4i.data.esgf.config.IEsgfConfig;
import nl.knmi.c4i.data.esgf.dto.DataNodeResultDTO;
import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.dto.SearchResultDTO;
import nl.knmi.c4i.data.esgf.request.ISearchQuery;
import nl.knmi.c4i.data.esgf.request.IUrlFactory;
import nl.knmi.c4i.data.esgf.response.SearchResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class EsgfProvider implements IEsgfProvider {

    private final IEsgfService service;
    private final IUrlFactory urlFactory;
    private IEsgfConfig config;

    @Value("${search.cache.location}")
    private String cacheLocation;
    @Value("${search.cache.validity_seconds}")
    private int cacheValiditySeconds;

    private ICache<String> cache;

    /**
     * @param service    Service which provides ESGF data
     * @param urlFactory Factory which creates url-query-string
     * @param config     provides configuration values
     */
    public EsgfProvider(IEsgfService service, IUrlFactory urlFactory, IEsgfConfig config) {
        this.service = service;
        this.urlFactory = urlFactory;

        this.config = config;
    }

    /**
     * Creates new Cache-object if not already present
     *
     * @return Cache-object
     */
    private ICache<String> getCache() {
        if (this.cache == null) {
            this.cache = new DiskCache(cacheLocation, cacheValiditySeconds);
        }
        return this.cache;
    }

    /**
     * @param query Search-Query that determines results
     *
     * @return SearchResponse with json-data, with forceRefresh set to false
     *
     * @throws IOException When JSON invalid
     */
    public SearchResponse provide(ISearchQuery query) throws IOException {
        return provide(query, false);
    }

    /**
     * @param query Search-Query that determines results
     *
     * @return SearchResponse with json-data
     *
     * @throws IOException When JSON invalid
     */
    public SearchResponse provide(ISearchQuery query, boolean forceRefresh) throws IOException {
        Predicate<String> blacklistHasFacet = facetName -> query.getFacetBlacklist()
                                                                .stream()
                                                                .map(FacetDTO::getShortName)
                                                                .anyMatch(facet -> facet.equalsIgnoreCase(facetName));
        this.config.getExcludedFacets()
                   .stream()
                   .filter(facet -> !blacklistHasFacet.test(facet))
                   .map(FacetDTO::new)
                   .forEach(query.getFacetBlacklist()::add);
        JSONObject json = this.getResponseData(query, forceRefresh);

        return processResponseData(json, query);
    }

    /**
     * Checks cache validity, returns if valid. If it's not valid, re-fetches data, caches it, returns data.
     *
     * @param query Search query
     *
     * @return Search data
     *
     * @throws IOException When service unreachable
     */
    private JSONObject getResponseData(ISearchQuery query, boolean forceRefresh) throws IOException {
        ICache<String> cache = this.getCache();
        try {
            if (cache.isCacheValid(query.getId()) && !forceRefresh) {
                String cacheContent = cache.get(query.getId());

                return new JSONObject(cacheContent);
            }
        } catch (JSONException ignored) {
            cache.invalidateCache(query.getId());
        }
        JSONObject json = service.fetch(query);

        if (query.getFacetWhitelist()
                 .isEmpty()) {
            Collection<FacetDTO> blacklist = query.getFacetBlacklist();

            blacklist.stream()
                     .map(FacetDTO::getShortName)
                     .forEach(json.getJSONObject("facet_counts")
                                  .getJSONObject("facet_fields")::remove);
        }

        cache.set(query.getId(), json.toString());

        return json;
    }

    /**
     * Processes response data into SearchResponse
     *
     * @param json  Json to process
     * @param query Query that provides data for SearchResponse
     *
     * @return SearchResponse
     *
     * @throws JSONException When json invalid
     */
    private SearchResponse processResponseData(JSONObject json, ISearchQuery query) throws JSONException, UnsupportedEncodingException {
        SearchResponse model = extractModel(json);

        model.getSearchResult()
             .setLimit(query.getLimit());
        model.getSearchResult()
             .setQuery(urlFactory.createSearchUrlQuery(query));

        return model;
    }

    /**
     * Extracts model from JSONObject
     *
     * @param object Object to extract from
     *
     * @return Extracted model
     *
     * @throws JSONException When JSON-items not present
     */
    private SearchResponse extractModel(JSONObject object) throws JSONException {
        Map<String, Map<String, Integer>> facetMap = new HashMap<>();

        JSONObject facetObject = object.getJSONObject("facet_counts")
                                       .getJSONObject("facet_fields");
        JSONObject responseObject = object.getJSONObject("response");

        Function<JSONArray, Map<String, Integer>> extractProperties = (JSONArray array) -> {
            Map<String, Integer> propertyResultCountMap = new HashMap<>();

            for (int propertyIndex = 0; propertyIndex < array.length(); propertyIndex += 2) {
                String property = (String) array.get(propertyIndex);
                int resultCount = (int) array.get(propertyIndex + 1);

                propertyResultCountMap.put(property, resultCount);
            }
            return propertyResultCountMap;
        };
        Consumer<String> registerToMap = (key) -> facetMap.put(key, extractProperties.apply(facetObject.getJSONArray(key)));

        //  Register them to the return-map
        facetObject.keySet()
                   .forEach(registerToMap);

        Collection<FacetDTO> facets = facetMap.entrySet()
                                              .stream()
                                              .map(entry -> new FacetDTO(entry.getKey(), entry.getValue()
                                                                                              .keySet()))
                                              .collect(Collectors.toList());

        SearchResultDTO searchResult = extractModelResults(responseObject);

        return new SearchResponse(facets, searchResult);
    }

    /**
     * Extracts results from model
     *
     * @param object Object to extract from
     *
     * @return SearchResultDTO with extracted data
     *
     * @throws JSONException When object-items not present
     */
    private SearchResultDTO extractModelResults(JSONObject object) throws JSONException {
        JSONArray resultArray = object.getJSONArray("docs");
        int numFound = object.getInt("numFound");

        Function<String, String> cleanUrl = (url) -> url.split("#")[0].split("\\|")[0];

        Collection<DataNodeResultDTO> dataNodeResults = new ArrayList<>();
        for (int resultIndex = 0; resultIndex < resultArray.length(); resultIndex++) {
            JSONObject resultObject = resultArray.getJSONObject(resultIndex);
            String dataNode, esgfId, id, url;
            dataNode = resultObject.getString("data_node");
            esgfId = resultObject.getString("master_id");
            id = dataNode + "::" + esgfId;
            url = cleanUrl.apply(resultObject.getJSONArray("url")
                                             .getString(0));

            dataNodeResults.add(new DataNodeResultDTO(dataNode, esgfId, id, url));
        }

        return new SearchResultDTO(dataNodeResults, numFound);
    }
}
