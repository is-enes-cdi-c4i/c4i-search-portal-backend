package nl.knmi.c4i.data.esgf.config;

import java.net.MalformedURLException;

public interface IEndpointConnector {
   /**
    * @param endpoint                  URL (string) of which the response code needs to be determined.
    * @return                          ResponseCode (int) based on given URL.
    * @throws MalformedURLException    When the given URL is malformed.
    */
   int getEndpointStatusCode(String endpoint) throws MalformedURLException;
}
