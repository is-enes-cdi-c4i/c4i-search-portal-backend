package nl.knmi.c4i.data.esgf.config;

import java.util.Map;

public class EsgfConfigModel {
    private String[] endpointURL;
    private int timeoutSeconds;
    private Map<String, String> facetDefaults;

    public String[] getEndpointURL() {
        return this.endpointURL;
    }

    public void setEndpointURL(String[] input) { this.endpointURL = input; }

    public int getTimeoutSeconds() {
        return this.timeoutSeconds;
    }

    public void setTimeoutSeconds(int input) { this.timeoutSeconds = input; }

    public Map<String, String> getFacetDefaults() { return this.facetDefaults; }

    public void setFacetDefaults(Map<String, String> facetDefaults) { this.facetDefaults = facetDefaults; }
}