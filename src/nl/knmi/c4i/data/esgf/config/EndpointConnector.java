package nl.knmi.c4i.data.esgf.config;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Service
public class EndpointConnector implements IEndpointConnector {
    /**
     * Opens a HttpURLConnection with RequestMethod 'GET', retrieves the response code from the connection and returns it.
     * If the connection cannot be made, it will be caught and return the 408 response code.
     *
     * @param endpoint                  URL (string) of which the response code needs to be determined.
     * @return                          ResponseCode (int) based on given URL.
     * @throws MalformedURLException    When the given URL is malformed.
     */
    @Override
    public int getEndpointStatusCode(String endpoint) throws MalformedURLException {
        URL url = new URL(endpoint);
        HttpURLConnection connection;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(3000);
            connection.connect();
            int code = connection.getResponseCode();
            connection.disconnect();
            return code;
        } catch (IOException e) {
            e.printStackTrace();
            return 408;
        }
    }
}
