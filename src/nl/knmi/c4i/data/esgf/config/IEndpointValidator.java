package nl.knmi.c4i.data.esgf.config;

import nl.knmi.c4i.data.esgf.exceptions.EndpointNotValidException;

import java.util.LinkedList;

public interface IEndpointValidator {
    /**
     * @param endpointUrls                  Contains a LinkedList (string) with endpoint URLs.
     * @return                              The first valid endpoint URL (response code 299 or lower).
     * @throws EndpointNotValidException    When none of the endpoints are valid.
     */
    String getFirstValidEndpoint(LinkedList<String> endpointUrls) throws EndpointNotValidException;
}
