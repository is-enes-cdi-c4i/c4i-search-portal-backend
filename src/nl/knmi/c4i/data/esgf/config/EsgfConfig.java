package nl.knmi.c4i.data.esgf.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.representer.Representer;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EsgfConfig implements IEsgfConfig {
    private Collection<String> endpointUrls;
    private int timeoutSeconds;
    @Value("${search.always_exclude_facets}")
    private String excludedFacets;
    @Value("${search.config_file}")
    private String configFilePath;

    public Collection<String> getEndpointUrls() {
        return this.endpointUrls;
    }

    public void setEndpointUrls(Collection<String> input) {
        this.endpointUrls = input;
    }

    public int getTimeoutSeconds() {
        return this.timeoutSeconds;
    }

    public void setTimeoutSeconds(int input) {
        this.timeoutSeconds = input;
    }

    @Override
    public Collection<String> getExcludedFacets() {
        return Arrays.stream(this.excludedFacets.split(","))
                     .map(String::trim)
                     .collect(Collectors.toList());
    }

    @Override
    public Map<String, String> getFacetDefaults() {
        EsgfConfigModel model = this.loadYaml();

        return model.getFacetDefaults() != null ? new HashMap<>(model.getFacetDefaults()) : new HashMap<>();
    }

    /**
     * Loads config.yaml as a EsgfConfigModel.
     * @return      EsgfConfigModel with values based on config.yaml.
     */
    public EsgfConfigModel loadYaml() {
        Representer representer = new Representer();
        representer.getPropertyUtils()
                   .setSkipMissingProperties(true);
        Yaml yaml = new Yaml(representer);
        InputStream inputStream = this.getClass()
                                      .getClassLoader()
                                      .getResourceAsStream(this.configFilePath);
        return yaml.loadAs(inputStream, EsgfConfigModel.class);
    }
}
