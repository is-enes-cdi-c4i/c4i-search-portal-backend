package nl.knmi.c4i.data.esgf.config;

import java.util.Collection;
import java.util.Map;

public interface IEsgfConfig {
    Collection<String> getEndpointUrls();

    int getTimeoutSeconds();

    Collection<String> getExcludedFacets();

    Map<String, String> getFacetDefaults();
}