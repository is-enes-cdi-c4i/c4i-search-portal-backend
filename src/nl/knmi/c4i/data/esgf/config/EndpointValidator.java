package nl.knmi.c4i.data.esgf.config;

import nl.knmi.c4i.data.esgf.exceptions.EndpointNotValidException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;

@Service
public class EndpointValidator implements IEndpointValidator {
    private IEndpointConnector endpointConnector;

    /**
     * @param endpointConnector             Contains the implementation of IEndpointConnector.
     */
    public EndpointValidator(IEndpointConnector endpointConnector) {
        this.endpointConnector = endpointConnector;
    }

    /**
     * Rotates through a list until it finds a valid endpoint (response code 299 or lower), and returns it.
     *
     * @param endpointUrls                  Contains a LinkedList (string) with endpoint URLs.
     * @return                              The first valid endpoint URL (response code 299 or lower).
     * @throws EndpointNotValidException    When none of the endpoints are valid.
     */
    @Override
    public String getFirstValidEndpoint(LinkedList<String> endpointUrls) throws EndpointNotValidException {
        for (String endpoint : endpointUrls) {
            try {
                if (endpointConnector.getEndpointStatusCode(endpoint) <= 299) {
                    return endpoint;
                }
            } catch (IOException ignored) {}
        }
        throw new EndpointNotValidException();
    }
}
