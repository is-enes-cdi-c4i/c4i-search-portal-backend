package nl.knmi.c4i.data.esgf.dto;

import java.util.ArrayList;
import java.util.Collection;

public class FacetDTO {
    private String shortName;
    private Collection<String> properties;

    public FacetDTO(String shortName) {
        this(shortName, new ArrayList<>());
    }

    public FacetDTO(String shortName, Collection<String> properties) {
        this.shortName = shortName;
        this.properties = properties;
    }

    public String getShortName() {
        return shortName;
    }

    public Collection<String> getProperties() {
        return properties;
    }

    public int getPropertyCount() {
        return properties.size();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FacetDTO)) return false;
        FacetDTO facet = (FacetDTO) obj;

        return facet.shortName.equals(this.shortName);
    }
}
