package nl.knmi.c4i.data.esgf.dto;

public class DataNodeResultDTO {
    private String dataNode, esgfId, id, url;

    public DataNodeResultDTO(String dataNode, String esgfId, String id, String url) {

        this.dataNode = dataNode;
        this.esgfId = esgfId;
        this.id = id;
        this.url = url;
    }

    public String getDataNode() {
        return dataNode;
    }

    public String getEsgfId() {
        return esgfId;
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }
}
