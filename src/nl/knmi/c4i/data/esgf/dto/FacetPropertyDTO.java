package nl.knmi.c4i.data.esgf.dto;

public class FacetPropertyDTO {
    private String name;
    private String facetName;

    public FacetPropertyDTO(String value, String facetName) {
        this.name = value;
        this.facetName = facetName;
    }

    public String getName() {
        return name;
    }

    public String getFacetName() {
        return facetName;
    }
}
