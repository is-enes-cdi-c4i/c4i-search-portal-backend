package nl.knmi.c4i.data.esgf.dto;

import java.util.Collection;

public class SearchResultDTO {
    private Collection<DataNodeResultDTO> results;
    private int resultFoundCount, limit;
    private String query;

    public SearchResultDTO(Collection<DataNodeResultDTO> results, int resultFoundCount) {
        this.results = results;
        this.resultFoundCount = resultFoundCount;
    }

    public Collection<DataNodeResultDTO> getResults() {
        return results;
    }

    public int getResultFoundCount() {
        return resultFoundCount;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
