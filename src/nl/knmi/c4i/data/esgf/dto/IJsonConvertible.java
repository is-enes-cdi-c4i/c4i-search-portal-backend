package nl.knmi.c4i.data.esgf.dto;

import org.json.JSONObject;

public interface IJsonConvertible {

    JSONObject toJson();
}
