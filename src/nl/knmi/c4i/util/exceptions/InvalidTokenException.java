package nl.knmi.c4i.util.exceptions;

public class InvalidTokenException extends Exception {
    private String message;
    /**
     *
     */
    private static final long serialVersionUID = 6797418585190698615L;

    public InvalidTokenException(String string) {
        message = string;
    }

    public String getMessage() {
        return message;
    }

}
