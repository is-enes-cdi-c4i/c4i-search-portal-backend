package nl.knmi.c4i.controllers;

import nl.knmi.c4i.data.esgf.IEsgfProvider;
import nl.knmi.c4i.data.esgf.dto.DataNodeResultDTO;
import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.dto.FacetPropertyDTO;
import nl.knmi.c4i.data.esgf.dto.SearchResultDTO;
import nl.knmi.c4i.data.esgf.exceptions.InvalidCacheException;
import nl.knmi.c4i.data.esgf.request.ISearchQuery;
import nl.knmi.c4i.data.esgf.request.SearchQuery;
import nl.knmi.c4i.util.Debug;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

@RestController
@RequestMapping("search")
@CrossOrigin
public class SearchController implements DisposableBean {
    private static ExecutorService threadPool = null;

    private IEsgfProvider esgfProvider;

    public SearchController(@Autowired IEsgfProvider esgfProvider) {
        super();

        this.esgfProvider = esgfProvider;
    }

    public void destroy() {
        Debug.println("Shutting down");
        threadPool.shutdown();
        esgfProvider = null;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<SearchResponseDTO> post(@RequestBody Map<String, Object> object) {
        Debug.println(object.toString());

        ISearchQuery query;
        try {
            Collection<FacetPropertyDTO> facetProperties = ((ArrayList<Map<String, String>>) object.get("facet_properties"))
                    .stream()
                    .map(map -> map.entrySet()
                                   .iterator()
                                   .next())
                    .map(entry -> new FacetPropertyDTO(entry.getValue(), entry.getKey()))
                    .collect(Collectors.toSet());

            int limit, offset;
            Collection<FacetDTO> facetWhitelist, facetBlacklist;

            limit = (int) object.getOrDefault("page_limit", 25);
            offset = ((int) object.getOrDefault("page", 0)) * limit;
            facetWhitelist = ((Collection<String>) object.getOrDefault("facet_whitelist", new ArrayList<>())).stream()
                                                                                                             .map(FacetDTO::new)
                                                                                                             .collect(Collectors.toSet());
            facetBlacklist = ((Collection<String>) object.getOrDefault("facet_blacklist", new ArrayList<>())).stream()
                                                                                                             .map(FacetDTO::new)
                                                                                                             .collect(Collectors.toSet());

            query = new SearchQuery(facetProperties, limit, offset, facetWhitelist, facetBlacklist, true);
        } catch (NullPointerException exception) {
            return ResponseEntity.badRequest()
                                 .body(null);
        }
        try {
            SearchResultDTO results = esgfProvider.provide(query)
                                                  .getSearchResult();
            SearchResponseDTO response = new SearchResponseDTO(results.getResults(), results.getResultFoundCount());
            // TODO return facets as well

            return ResponseEntity.ok(response);
        } catch (InvalidCacheException | IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                                 .body(null);
        }
    }

    public static class SearchResponseDTO {
        private Collection<DataNodeResultDTO> results;
        private int numberFound;

        public SearchResponseDTO(Collection<DataNodeResultDTO> results, int numberFound) {
            this.results = results;
            this.numberFound = numberFound;
        }

        public Collection<DataNodeResultDTO> getResults() {
            return this.results;
        }

        public void setResults(Collection<DataNodeResultDTO> results) {
            this.results = results;
        }

        public int getNumberFound() {
            return this.numberFound;
        }

        public void setNumberFound(int numberFound) {
            this.numberFound = numberFound;
        }
    }
}
