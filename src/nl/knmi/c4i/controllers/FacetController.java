package nl.knmi.c4i.controllers;

import nl.knmi.c4i.data.esgf.IEsgfProvider;
import nl.knmi.c4i.data.esgf.dto.FacetDTO;
import nl.knmi.c4i.data.esgf.exceptions.InvalidCacheException;
import nl.knmi.c4i.data.esgf.request.ISearchQuery;
import nl.knmi.c4i.data.esgf.request.SearchQuery;
import nl.knmi.c4i.util.Debug;
import nl.knmi.c4i.util.Tuple;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

@RestController
@RequestMapping("facets")
@CrossOrigin
public class FacetController implements DisposableBean {
    private static ExecutorService threadPool = null;

    private IEsgfProvider esgfProvider;

    public FacetController(@Autowired IEsgfProvider esgfProvider) {
        super();

        this.esgfProvider = esgfProvider;
    }

    public void destroy() {
        Debug.println("Shutting down");
        threadPool.shutdown();
        esgfProvider = null;
    }

    @ResponseBody
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<FacetDTO>> get() {
        Debug.println("");

        try {
            Collection<FacetDTO> facets = esgfProvider.provide(SearchQuery.emptyQuery())
                                                      .getFacets();
            return ResponseEntity.ok(facets);
        } catch (InvalidCacheException | IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                                 .body(null);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/ids", method = RequestMethod.GET)
    public ResponseEntity<Collection<String>> getIds() {
        Debug.println("");

        try {
            List<String> facetNames = esgfProvider.provide(SearchQuery.emptyQuery())
                                                  .getFacets()
                                                  .stream()
                                                  .map(FacetDTO::getShortName)
                                                  .sorted()
                                                  .collect(Collectors.toList());

            return ResponseEntity.ok(facetNames);
        } catch (InvalidCacheException | IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                                 .body(null);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/counts", method = RequestMethod.GET)
    public ResponseEntity<Collection<Tuple<String, Integer>>> getPropertyCounts() {
        Debug.println("");

        try {
            List<Tuple<String, Integer>> facetNames = esgfProvider.provide(SearchQuery.emptyQuery())
                                                                  .getFacets()
                                                                  .stream()
                                                                  .map(facet -> new Tuple<>(facet.getShortName(), facet.getPropertyCount()))
                                                                  .sorted(Comparator.comparing(Tuple::getSecond))
                                                                  .collect(Collectors.toList());

            return ResponseEntity.ok(facetNames);
        } catch (InvalidCacheException | IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                                 .body(null);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<FacetDTO> get(@PathVariable String id) {

        ISearchQuery query = SearchQuery.emptyQuery();
        query.getFacetWhitelist()
             .add(new FacetDTO(id));

        List<FacetDTO> facets;
        try {
            facets = new ArrayList<>(esgfProvider.provide(query)
                                                 .getFacets());
        } catch (InvalidCacheException | IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                                 .body(null);
        }
        if (facets.isEmpty()) {
            return ResponseEntity.notFound()
                                 .build();
        }

        return ResponseEntity.ok(facets.get(0));
    }
}
