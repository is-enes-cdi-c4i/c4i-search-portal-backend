package nl.knmi.c4i.tasks;

import nl.knmi.c4i.data.esgf.EsgfProvider;
import nl.knmi.c4i.data.esgf.request.ISearchQuery;
import nl.knmi.c4i.data.esgf.request.SearchQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class PrecacheTask {
    @Autowired
    private EsgfProvider provider;

    /**
     * Refreshes initial query cache at a certain configured interval.
     */
    @Scheduled(fixedRateString = "#{ T(Long).parseLong('${search.cache.refresh_seconds}') * 1000}")
    public void refreshCache() {
        ISearchQuery query = SearchQuery.emptyQuery();
        try {
            provider.provide(query, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
