package nl.knmi.c4i;

import nl.knmi.c4i.util.Debug;
import nl.knmi.c4i.util.startup.ElementNotFoundException;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;
import java.security.Security;
import java.util.Properties;

@PropertySource("application.properties")
@SpringBootApplication
@EnableScheduling
public class C4ISearchPortalApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        try {
            return application.sources(C4ISearchPortalApplication.class).properties(getProperties());
        } catch (ElementNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        Debug.println("Error");
        return null;
    }

    public static void main(String[] args) {
        try {
            configureApplication(new SpringApplicationBuilder()).properties(getProperties()).run(args);
        } catch (ElementNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            Debug.errprintln(e.getMessage());
        }
    }

    private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder builder) {
        try {
            return builder.sources(C4ISearchPortalApplication.class)
                          .properties(getProperties())
                          .bannerMode(Banner.Mode.OFF);
        } catch (ElementNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    static Properties getProperties() throws ElementNotFoundException, IOException {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        Properties props = new Properties();

        /*
         * server.port is the default spring tomcat connector, it can be both used for http and https
         * server.http.port is the adaguc-services added connector, it is meant for http only.
         * For backwards compatibility with previous releases, http.port is used for https and server.http.port is used for http *
         */

        props.put("spring.http.multipart.max-file-size", "100MB");
        props.put("spring.http.multipart.max-request-size", "100MB");

        return props;
    }
}
