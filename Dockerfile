FROM centos/devtoolset-7-toolchain-centos7:7
USER root

MAINTAINER Climate4Impact Team at KNMI <c4i@knmi.nl>

RUN yum update -y && yum install -y \
    maven
    
WORKDIR /c4i-search-portal-backend
COPY /src/  /c4i-search-portal-backend/src/
COPY pom.xml /c4i-search-portal-backend/pom.xml
RUN mvn package
RUN cp /c4i-search-portal-backend/target/c4i-search-backend-*.jar /c4i-search-portal-backend/c4i-search-portal-backend.jar

# For HTTP
EXPOSE 8080 

COPY ./Docker/start.sh /c4i-search-portal-backend/start.sh
RUN chmod a+x /c4i-search-portal-backend/start.sh

ENTRYPOINT /c4i-search-portal-backend/start.sh
